Lane Koistinen
Frequency Adjuster Programmed in Assembly Language 

When program is run, input on PORTB<2:0> is used to set the frequency on RA0 to one of three settings. This does not change the frequency of the clock, but rather, the frequency of interrupts being processed by the program, and the corresponding frequency on RA0. The three settings on PORTB<2:0> are shown below.
   State 3: 100 processes every interrupt (every ms)
   State 2: 010 processes every other interrupt (every other ms)
   State 1: 001 processes every fourth interrupt (every 4th ms)
If more than one of bits <2:0> are set, the lowest frequency will be prioritized. Note: the power on RA0 will be set to 50%.
