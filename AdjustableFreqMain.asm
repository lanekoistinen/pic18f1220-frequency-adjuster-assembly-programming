 
; Lane Koistinen
; Updated 08.29.2020
; PIC18F1220 Assembly Language Program
;
; Program outputs to RA0 with 3 frequency levels 
; configured by user input on PORTB
; 
; INPUT VALUES ON PORTB
;   xxxxx001 : RA0 frequency = 0.25 * clock instruction frequency (state 1)
;   xxxxx010 : RA0 frequency = 0.50 * clock instruction frequency (state 2)
;   xxxxx100 : RA0 frequency = 1.00 * clock instruction frequency (state 3)
  

    list p=18f1220 
    radix hex 
    config WDT=OFF,LVP=OFF,OSC=INTIO2

#include p18f1220.inc 
#define stateLSB 0x80 
#define stateMSB 0x81
#define intCnt   0x83

   org 0x000 
   GOTO main

   org 0x008 ; Executes after high priority interrupt
   GOTO HPRIO

   org 0x20 
main:
   ; initialize all I/O ports
   CLRF stateLSB ; state variable least sig bits
   CLRF stateMSB ; state variable most sig bits
   CLRF intCnt
   CLRF PORTA 
   CLRF PORTB 
   MOVLW 0x7F
   MOVWF ADCON1 ; Set all digital except MSB
   MOVLW 0xFE 
   MOVWF TRISA
   MOVLW 0xFF
   MOVWF TRISB 
   MOVLW 0x60
   IORWF OSCCON ; freq = 4 MHz

   ; Configure Interrupts
   ;
   ; Enable Peripherals,TMR0 interrupt, priority levels,
   ; global interrupts, etc.
   BSF INTCON, PEIE 
   BSF INTCON, TMR0IE 
   BSF INTCON2, TMR0IP 
   BSF RCON, IPEN 
   BCF INTCON, TMR0IF 
   MOVLW 0x41 
   MOVWF T0CON
   CLRF TMR0L
   BSF T0CON,TMR0ON 
   BSF INTCON, GIE 

; Main execution only acts upon
; receiving an interrupt
; Otherwise, it does nothing.
MainL: 
   BRA MainL

; Interrupt Handling Section
HPRIO: ; High priority interrupts including Timer 0 Int.
   BTFSC INTCON, TMR0IF ; Check for Timer 0 high priority Interrupt
   BRA TMR0int
   BRA FINISHINT ; return from interrupt
   
TMR0int: ; handle Timer 0 Interrupt
   INCF intCnt ; Count interrupts with 8-bit reg
   ; Check state based on PORTB<2:0>
   BTFSC PORTB, 0
   BRA S1
   BTFSC PORTB, 1
   BRA S2
   BTFSC PORTB, 2
   BRA S3    
   
TMR0CONT:
   ; 50% Power 
   BTFSC stateLSB, 0 ; toggle every other time
   BTG PORTA, 0
   ; Increment counter each acknowledged interrupt
   INCF stateLSB
   TSTFSZ stateLSB
   BRA FINISHINT
   INCF stateMSB
   BTFSC stateMSB, 0
   BRA FINISHINT

; Return from interrupt
FINISHINT:
   BCF INTCON, TMR0IF ; Clear interrupt flag
   RETFIE

S1: ; Only listen to every fourth interrupt
   MOVF intCnt, 0
   ANDLW 0x03
   XORLW 0x03
   BZ TMR0CONT
   BRA FINISHINT

S2: ; Listen to every other interrupt
   BTFSC intCnt, 0
   BRA FINISHINT
   BRA TMR0CONT

S3: ; Listen to all interrupts
   BRA TMR0CONT

   end



